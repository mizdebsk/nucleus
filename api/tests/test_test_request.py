import json
import subprocess
from copy import deepcopy
from datetime import datetime
from time import sleep
from uuid import uuid4

import pkg_resources
import pytest

from tft.nucleus.api.config import settings
from tft.nucleus.api.core.database import Request, RequestStateType, SessionLocal
from tft.nucleus.api.public import api

from .conftest import add_request, assets, internal_client, public_client


def test_create_test_request(token):
    """
    Test the create test request endpoint.
    """
    response = public_client.post('/v0.1/requests', json=assets['test_request_example'])
    content = json.loads(response.content)

    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    expected_response = assets['create_test_request_response_example'].copy()
    expected_response['id'] = content['id']
    expected_response['created'] = content['created']
    expected_response['updated'] = content['updated']

    assert content == expected_response


def test_create_test_request_bad_compose(token):
    """
    Test the create test request endpoint with bad compose.
    """
    settings.COMPOSE_DOCS = "some-docs"

    response = public_client.post('/v0.1/requests', json=assets['test_request_bad_compose_example'])
    content = json.loads(response.content)

    assert response.status_code == 400

    assert content['message'] == "Compose Fedora-9000 does not exist. Go to some-docs to find all available composes."


def test_create_test_request_regex_compose(token):
    """
    Test the create test request endpoint with regex compose.
    """
    response = public_client.post('/v0.1/requests', json=assets['test_request_regex_compose_example'])
    content = json.loads(response.content)

    assert response.status_code == 200

    assert content['environments'][0]['os']['compose'] == 'regex-compose-foo-x86_64'


def test_create_test_request_no_compose(token):
    """
    Test the create test request endpoint with regex compose.
    """
    response = public_client.post('/v0.1/requests', json=assets['test_request_no_compose_example'])

    assert response.status_code == 422


def test_create_test_request_too_much_test_types(token):
    """
    Test the create test request endpoint with too much tests types.
    """
    response = public_client.post("/v0.1/requests", json=assets['test_request_too_much_test_types'])
    content = json.loads(response.content)

    assert response.status_code == 422
    assert content['message'] == 'Test section has more than one type.'


def test_create_test_request_no_test(token):
    """
    Test the create test request endpoint with no tests.
    """
    response = public_client.post('/v0.1/requests', json=assets['test_request_no_tests'])
    content = json.loads(response.content)

    assert response.status_code == 422
    assert content['message'] == 'Test section is empty or test type is wrong.'


def test_create_test_request_extra_fields(token):
    """
    Test the create test request endpoint with extra fields. It should be forbidden.
    """
    test_request = deepcopy(assets['test_request_example'])
    test_request['test_extra_key'] = 'test_extra_value'
    response = public_client.post('/v0.1/requests', json=test_request)
    content = json.loads(response.content)

    assert response.status_code == 422
    assert content['detail'][0]['msg'] == 'extra fields not permitted'


def test_get_test_request_public(token):
    """
    Test the get test request endpoint.
    """
    response = public_client.post('/v0.1/requests', json=assets['test_request_example'])
    create_response_content = json.loads(response.content)

    assert response.status_code == 200

    response = public_client.get('/v0.1/requests/{}'.format(create_response_content['id']))
    get_response_content = json.loads(response.content)

    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    expected_response = deepcopy(assets['get_test_request_response_example_public'])
    expected_response['id'] = get_response_content['id']
    expected_response['created'] = get_response_content['created']
    expected_response['updated'] = get_response_content['updated']
    expected_response['token_id'] = get_response_content['token_id']
    expected_response['user_id'] = get_response_content['token_id']
    assert get_response_content == expected_response


def test_get_test_request_internal(token):
    """
    Test the get test request endpoint.
    """
    response = internal_client.post('/v0.1/requests', json=assets['test_request_example'])
    create_response_content = json.loads(response.content)

    assert response.status_code == 200

    response = internal_client.get('/v0.1/requests/{}'.format(create_response_content['id']))

    assert response.status_code == 422

    response = internal_client.get(
        '/v0.1/requests/{}?api_key={}'.format(create_response_content['id'], assets['api_key'])
    )
    get_response_content = json.loads(response.content)

    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    expected_response = deepcopy(assets['get_test_request_response_example'])
    expected_response['id'] = get_response_content['id']
    expected_response['created'] = get_response_content['created']
    expected_response['updated'] = get_response_content['updated']
    expected_response['token_id'] = get_response_content['token_id']
    expected_response['user_id'] = get_response_content['token_id']

    # Add notification to the expected response - it is an additional field compared to the public endpoint
    expected_response['notification'] = {'webhook': {'url': 'http://example.com/webhook', 'token': ''}}

    assert get_response_content == expected_response


def test_get_test_request_not_exists(token):
    """
    Test the get test request endpoint with not existing test request.
    """
    response = public_client.get('/v0.1/requests/5e64028c-b17e-4fd8-b16e-5fffa881ec31')
    content = json.loads(response.content)

    assert response.status_code == 404

    assert content['message'] == 'No such entity'


def test_get_test_requests(token, delete_all_requests):
    """
    Test the get requests with valid state.
    """
    response = public_client.post('/v0.1/requests', json=assets['test_request_example'])
    assert response.status_code == 200
    response = public_client.post('/v0.1/requests', json=assets['test_request_example'])
    assert response.status_code == 200

    response = public_client.get('/v0.1/requests?state=new')
    get_response_content = json.loads(response.content)

    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    expected_response = [
        deepcopy(assets['get_test_request_response_example']),
        deepcopy(assets['get_test_request_response_example']),
    ]
    for i in range(2):
        expected_response[i]['id'] = get_response_content[i]['id']
        expected_response[i]['created'] = get_response_content[i]['created']
        expected_response[i]['updated'] = get_response_content[i]['updated']
        expected_response[i]['token_id'] = get_response_content[i]['token_id']
        expected_response[i]['user_id'] = get_response_content[i]['token_id']
        # Secrets should not be visible in public api
        expected_response[i]['test']['fmf']['url'] = 'https://*****@example.com'
        expected_response[i]['environments_requested'][0]['secrets'] = None
        expected_response[i]['environments_requested'][0]['tmt']['environment'] = None

    assert get_response_content == expected_response


def test_get_test_requests_non_valid_states(token):
    """
    Test the get requests with non-valid states.
    """
    # State is empty
    response = public_client.get('/v0.1/requests?state=running')
    get_response_content = json.loads(response.content)

    assert response.status_code == 200
    assert get_response_content is None

    # State does not exist
    response = public_client.get('/v0.1/requests?state=foo')
    get_response_content = json.loads(response.content)

    assert response.status_code == 200
    assert get_response_content is None

    # No state
    response = public_client.get('/v0.1/requests')
    get_response_content = json.loads(response.content)

    assert response.status_code == 200
    assert get_response_content is None


def test_get_request_token_id(delete_all_tokens, token):
    # Add a request
    public_client.post('/v0.1/requests', json=assets['test_request_example'])

    # Filter requests by a valid user id
    response = public_client.get('/v0.1/requests?token_id={}'.format(token.id))
    get_response_content = json.loads(response.content)

    assert response.status_code == 200
    assert len(get_response_content) == 1
    assert get_response_content[0]['token_id'] == token.id

    # Filter requests by a nonexistent user id
    response = public_client.get('/v0.1/requests?token_id=11111111-1111-1111-1111-111111111111')
    get_response_content = json.loads(response.content)

    assert response.status_code == 200
    assert get_response_content is None

    # Filter requests by an invalid user id
    response = public_client.get('/v0.1/requests?token_id=abc123')
    get_response_content = json.loads(response.content)

    assert response.status_code == 422
    assert get_response_content['detail'][0]['msg'] == 'value is not a valid uuid'


def test_get_request_get_request_created(delete_all_tokens, token):
    # Add requests
    request_ids = [
        '11111111-1111-1111-1111-111111111111',
        '22222222-2222-2222-2222-222222222222',
        '33333333-3333-3333-3333-333333333333',
    ]
    for request_id in request_ids:
        add_request(
            Request(
                id=request_id,
                token_id=token.id,
                environments_requested=[],
                test={'fmf': {'url': 'https://example.com/repo'}},
            )
        )

    # Modify their `created` time to something static
    session = SessionLocal()
    request = session.query(Request).filter(Request.id == request_ids[0]).first()
    request.created = '2023-09-01T10:10:10.123456'
    request = session.query(Request).filter(Request.id == request_ids[1]).first()
    request.created = '2023-09-02T10:10:10.123456'
    request = session.query(Request).filter(Request.id == request_ids[2]).first()
    request.created = '2023-09-02T15:15:15.123456'
    session.commit()
    session.close()

    # Filter using `created_after`
    response = public_client.get('/v0.1/requests?token_id={}&created_after=2023-09-02'.format(token.id))
    get_response_content = json.loads(response.content)

    assert len(get_response_content) == 2
    assert get_response_content[0]['id'] == request_ids[1]
    assert get_response_content[1]['id'] == request_ids[2]

    # Filter using `created_before`
    response = public_client.get('/v0.1/requests?token_id={}&created_before=2023-09-02 12:00:00'.format(token.id))
    get_response_content = json.loads(response.content)

    assert len(get_response_content) == 2
    assert get_response_content[0]['id'] == request_ids[0]
    assert get_response_content[1]['id'] == request_ids[1]

    # Filter using `created_after` and `created_before`
    response = public_client.get(
        '/v0.1/requests?token_id={}&created_after=2023-09-02&created_before=2023-09-02 12:00:00'.format(token.id)
    )
    get_response_content = json.loads(response.content)

    assert len(get_response_content) == 1
    assert get_response_content[0]['id'] == request_ids[1]

    response = public_client.get(
        '/v0.1/requests?token_id={}&created_after=2023-09-01&created_before=2023-09-01'.format(token.id)
    )
    get_response_content = json.loads(response.content)

    assert response.status_code == 200
    assert get_response_content is None


@pytest.mark.parametrize(
    'current_states, valid_states, invalid_states',
    [
        # general - transition all states except canceled and cancel-requested
        (
            [
                RequestStateType.NEW,
                RequestStateType.QUEUED,
                RequestStateType.RUNNING,
                RequestStateType.COMPLETE,
                RequestStateType.ERROR,
            ],
            [
                RequestStateType.NEW,
                RequestStateType.QUEUED,
                RequestStateType.RUNNING,
                RequestStateType.COMPLETE,
                RequestStateType.ERROR,
                RequestStateType.CANCEL_REQUESTED,
            ],
            [
                RequestStateType.CANCELED,
            ],
        ),
        # canceled - transition from cancel state
        (
            [
                RequestStateType.CANCELED,
            ],
            [
                RequestStateType.CANCELED,
            ],
            [
                RequestStateType.NEW,
                RequestStateType.QUEUED,
                RequestStateType.RUNNING,
                RequestStateType.COMPLETE,
                RequestStateType.ERROR,
                RequestStateType.CANCEL_REQUESTED,
            ],
        ),
        # cancel-requested - transition from cancel-requested state
        (
            [
                RequestStateType.CANCEL_REQUESTED,
            ],
            [
                RequestStateType.CANCEL_REQUESTED,
                RequestStateType.CANCELED,
            ],
            [
                RequestStateType.NEW,
                RequestStateType.QUEUED,
                RequestStateType.RUNNING,
                RequestStateType.COMPLETE,
                RequestStateType.ERROR,
            ],
        ),
    ],
    ids=['general', 'canceled', 'cancel-requested'],
)
def test_update_test_request_state_transitions(token, current_states, valid_states, invalid_states):
    """
    Test the correct behaviour of state transitions for test request updates.
    """

    session = SessionLocal()

    def test_transition(current_state, future_state):
        response = internal_client.post('/v0.1/requests', json=assets['test_request_example'])
        create_response_content = json.loads(response.content)
        assert response.status_code == 200

        # set current state via database to mitigate issues with state transition
        session.query(Request).update({'state': current_state})
        session.commit()

        # set future state
        response = internal_client.put(
            '/v0.1/requests/{}'.format(create_response_content['id']),
            json={'api_key': assets['api_key'], 'state': str(future_state)},
        )
        return response

    for current_state in current_states:
        for valid_state in valid_states:
            print(f"testing for valid transition {current_state} -> {valid_state}")
            response = test_transition(current_state, valid_state)
            assert response.status_code == 200

        for invalid_state in invalid_states:
            print(f"testing for invalid transition {current_state} -> {invalid_state}")
            response = test_transition(current_state, invalid_state)
            assert response.status_code == 409
            expected_message = f"Request in state '{str(current_state)}' cannot be changed to '{str(invalid_state)}'."
            assert response.json().get('message') == expected_message


def test_update_test_request_queued_run_time(token):
    """
    Test the update test request endpoint if it fills in queued_time.
    """
    response = internal_client.post('/v0.1/requests', json=assets['test_request_example'])
    create_response_content = json.loads(response.content)

    assert response.status_code == 200

    sleep(5)  # imitation of queue

    response = internal_client.put(
        '/v0.1/requests/{}'.format(create_response_content['id']), json=assets['test_request_running_update']
    )
    content = json.loads(response.content)

    assert response.status_code == 200
    assert content['state'] == 'running'
    assert content['queued_time'] is not None
    # Check if queued_time changed to something
    assert content['queued_time'] >= 5
    # Check if queued_time do not have problems with time zones
    assert content['queued_time'] < 10

    sleep(5)  # imitation of running

    response = internal_client.put(
        '/v0.1/requests/{}'.format(create_response_content['id']), json=assets['test_request_complete_update']
    )
    content = json.loads(response.content)

    assert response.status_code == 200
    assert content['state'] == 'complete'
    # Check if queued_time changed to something
    assert content['run_time'] >= 10
    # Check if queued_time do not have problems with time zones
    assert content['run_time'] < 15


def test_artifact_without_install(token):
    # This test excercises the old variant of request without '"install": true/false' value in artifacts. These requests
    # are present in the production database, but it is impossible to post them using the current API, therefore we have
    # to add them to the database manually.

    request = Request(
        id=str(uuid4()),
        token_id=token.id,
        environments_requested=[
            {
                "arch": "x86_64",
                "os": {"compose": "Fedora-Rawhide"},
                "artifacts": [{"id": "7654321:fedora-38-x86_64", "type": "fedora-copr-build"}],
            }
        ],
        test={"fmf": {"url": "https://example.com/repo"}},
    )
    add_request(request)

    response = public_client.get('/v0.1/requests/{}'.format(request.id))
    content = json.loads(response.content)

    expected_response = deepcopy(assets['get_test_artifact_without_install_response'])
    expected_response['id'] = request.id
    expected_response['token_id'] = token.id
    expected_response['user_id'] = token.id
    expected_response['created'] = datetime.strftime(request.created, '%Y-%m-%dT%H:%M:%S.%f')
    expected_response['updated'] = datetime.strftime(request.updated, '%Y-%m-%dT%H:%M:%S.%f')

    assert response.status_code == 200
    assert content == expected_response


def test_settings_pipeline_name_tmt_multihost(token):
    # Invalid request - STI test with tmt-multihost
    request = deepcopy(assets['test_request_sti'])
    request['settings'] = {'pipeline': {'type': 'tmt-multihost'}}

    response = public_client.post("/v0.1/requests", json=request)
    content = json.loads(response.content)

    assert response.status_code == 400
    assert (
        content['message'] == 'The value `tmt-multihost` of option `settings.pipeline.type` is incompatible with STI '
        'test type.'
    )

    # Valid request
    request = deepcopy(assets['test_request_example'])
    request['settings'] = {'pipeline': {'type': 'tmt-multihost'}}

    response = public_client.post('/v0.1/requests', json=request)
    content = json.loads(response.content)

    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    expected_response = assets['create_test_request_response_example'].copy()
    expected_response['id'] = content['id']
    expected_response['created'] = content['created']
    expected_response['updated'] = content['updated']
    expected_response['settings'] = {
        'pipeline': {
            'provision-error-failed-result': False,
            'type': 'tmt-multihost',
            'timeout': None,
            'parallel-limit': None,
        },
        'worker': None,
    }

    assert content == expected_response


def test_settings_pipeline_error_failed_result(token):
    request = deepcopy(assets['test_request_settings_provision_error'])

    response = public_client.post("/v0.1/requests", json=request)
    content = json.loads(response.content)

    # The request should be valid
    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    assert content['settings'] == {
        'pipeline': {'provision-error-failed-result': True, 'timeout': None, 'type': None, 'parallel-limit': None},
        'worker': None,
    }


@pytest.mark.parametrize(
    'parallel_limit, expected_status_code',
    [
        (0, 422),
        (0.5, 422),
        (1, 200),
        (64, 200),
        (65, 422),
    ],
)
def test_settings_pipeline_error_pipeline_limit(token, parallel_limit, expected_status_code):
    request = deepcopy(assets['test_request_example'])
    request['settings'] = {'pipeline': {'parallel-limit': parallel_limit}}

    response = public_client.post("/v0.1/requests", json=request)

    assert response.status_code == expected_status_code


def test_delete_test_request():
    """
    Test the delete test request endpoint which cancels the request.
    """
    response = public_client.post('/v0.1/requests', json=assets['test_request_example'])
    create_response_content = json.loads(response.content)

    assert response.status_code == 200

    # missing api key - validation error
    response = public_client.delete('/v0.1/requests/11111111-1111-1111-1111-111111111111')
    assert response.status_code == 422

    # invalid api key
    response = public_client.request(
        method='DELETE', url='/v0.1/requests/{}'.format(create_response_content['id']), json={'api_key': 'invalid'}
    )
    content = json.loads(response.content)
    assert response.status_code == 401
    assert content['message'] == 'Not authorized to perform this action'

    # invalid request
    response = public_client.request(
        method='DELETE', url='/v0.1/requests/0d8b5d17-a49f-4a76-a463-1d2f7bc23f5f', json={'api_key': assets['api_key']}
    )
    content = json.loads(response.content)
    assert response.status_code == 404
    assert content['message'] == 'No such entity'

    # cancel-requested
    response = public_client.request(
        method='DELETE',
        url='/v0.1/requests/{}'.format(create_response_content['id']),
        json={'api_key': assets['api_key']},
    )
    content = json.loads(response.content)
    assert response.status_code == 200
    expected_response = deepcopy(assets['get_test_request_response_example_public'])
    expected_response['state'] = 'cancel-requested'
    expected_response['id'] = content['id']
    expected_response['created'] = content['created']
    expected_response['updated'] = content['updated']
    expected_response['token_id'] = content['token_id']
    expected_response['user_id'] = content['token_id']
    assert content == expected_response

    # already cancel requested
    response = public_client.request(
        method='DELETE',
        url='/v0.1/requests/{}'.format(create_response_content['id']),
        json={'api_key': assets['api_key']},
    )
    assert response.status_code == 204
