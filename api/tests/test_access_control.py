import json
from copy import deepcopy

import pytest

from .conftest import assets, delete_all_tokens, internal_client, public_client

#
# Test access to request operations
#


@pytest.mark.parametrize(
    'api_key, status_code',
    [
        (assets['api_key'], 200),
        (assets['worker_role_api_key'], 200),
        (assets['user_role_api_key'], 200),
        (assets['banned_role_api_key'], 403),
        (assets['bad_api_key'], 401),
        (None, 422),
    ],
)
def test_access_create_test_request(token, user_role_token, worker_role_token, banned_role_token, api_key, status_code):
    """
    Test access to the create test request endpoint.
    """
    request = deepcopy(assets['test_request_example'])

    request['api_key'] = api_key
    response = public_client.post('/v0.1/requests', json=request)
    assert response.status_code == status_code


@pytest.mark.parametrize(
    'api_key, status_code',
    [
        (assets['api_key'], 200),
        (assets['worker_role_api_key'], 200),
        (assets['user_role_api_key'], 403),
        (assets['banned_role_api_key'], 403),
        (assets['bad_api_key'], 401),
        (None, 422),
    ],
)
def test_access_get_test_request_internal(
    token, user_role_token, worker_role_token, banned_role_token, api_key, status_code
):
    """
    Test access to the get test request endpoint.
    """
    response = internal_client.post('/v0.1/requests', json=assets['test_request_example'])
    assert response.status_code == 200

    request_id = json.loads(response.content)['id']

    response = internal_client.get(
        '/v0.1/requests/{}{}'.format(request_id, '?api_key={}'.format(api_key) if api_key else '')
    )
    assert response.status_code == status_code


@pytest.mark.parametrize(
    'api_key, status_code',
    [
        (assets['api_key'], 200),
        (assets['worker_role_api_key'], 200),
        (assets['user_role_api_key'], 403),
        (assets['banned_role_api_key'], 403),
        (assets['bad_api_key'], 401),
        (None, 422),
    ],
)
def test_access_update_test_request(token, user_role_token, worker_role_token, banned_role_token, api_key, status_code):
    """
    Test access to the update test request endpoint.
    """
    response = internal_client.post('/v0.1/requests', json=assets['test_request_example'])
    request_id = json.loads(response.content)['id']
    assert response.status_code == 200

    response = internal_client.put(
        '/v0.1/requests/{}'.format(request_id),
        json={'api_key': api_key, 'notes': [{'level': 'info', 'message': 'test'}]},
    )
    assert response.status_code == status_code


@pytest.mark.parametrize(
    'api_key, status_code',
    [
        (assets['api_key'], 200),
        (assets['worker_role_api_key'], 200),
        (assets['user_role_api_key'], 403),
        (assets['banned_role_api_key'], 403),
        (assets['bad_api_key'], 401),
        (None, 422),
    ],
)
def test_access_delete_test_request(token, user_role_token, worker_role_token, banned_role_token, api_key, status_code):
    """
    Test access to the delete test request endpoint which cancels the request.
    """
    response = public_client.post('/v0.1/requests', json=assets['test_request_example'])
    request_id = json.loads(response.content)['id']
    assert response.status_code == 200

    response = public_client.request(
        method='DELETE',
        url='/v0.1/requests/{}'.format(request_id),
        json={'api_key': api_key},
    )
    assert response.status_code == status_code


@pytest.mark.parametrize(
    'api_key, status_code',
    [
        (assets['api_key'], 200),
        (assets['worker_role_api_key'], 200),
        (assets['user_role_api_key'], 200),
    ],
)
def test_access_delete_owned_test_request(
    token, user_role_token, worker_role_token, banned_role_token, api_key, status_code
):
    """
    Deleting owned request should pass for user and stronger roles
    """
    request = deepcopy(assets['test_request_example'])
    request['api_key'] = api_key
    response = public_client.post('/v0.1/requests', json=request)
    request_id = json.loads(response.content)['id']
    assert response.status_code == 200

    response = public_client.request(
        method='DELETE',
        url='/v0.1/requests/{}'.format(request_id),
        json={'api_key': api_key},
    )
    assert response.status_code == status_code


#
# User operations
#
@pytest.mark.parametrize(
    'api_key, status_code',
    [
        (assets['api_key'], 200),
        (assets['worker_role_api_key'], 403),
        (assets['user_role_api_key'], 403),
        (assets['banned_role_api_key'], 403),
        (assets['bad_api_key'], 401),
        (None, 422),
    ],
)
def test_access_create_token(user, token, user_role_token, worker_role_token, banned_role_token, api_key, status_code):
    """
    Test access to the create token endpoint.
    """
    request = deepcopy(assets['create_token_example'])

    request['user_id'] = user.id
    request['api_key'] = api_key
    response = internal_client.post('/v0.1/tokens', json=request)
    assert response.status_code == status_code


@pytest.mark.parametrize(
    'api_key, status_code',
    [
        (assets['api_key'], 200),
        # user can get his own user
        (assets['create_token_example']['token_api_key'], 200),
        (assets['worker_role_api_key'], 200),
        (assets['user_role_api_key'], 403),
        (assets['banned_role_api_key'], 403),
        (assets['bad_api_key'], 401),
        (None, 422),
    ],
)
def test_access_get_token(
    user, delete_all_tokens, token, user_role_token, worker_role_token, banned_role_token, api_key, status_code
):
    """
    Test access to the get user endpoint.
    """
    request = deepcopy(assets['create_token_example'])
    request['user_id'] = user.id
    response = internal_client.post('/v0.1/tokens', json=request)
    assert response.status_code == 200
    token_id = json.loads(response.content)['id']

    response = internal_client.get(
        '/v0.1/tokens/{}{}'.format(token_id, '?api_key={}'.format(api_key) if api_key else '')
    )
    assert response.status_code == status_code


@pytest.mark.parametrize(
    'api_key, status_code',
    [
        (assets['api_key'], 200),
        (assets['worker_role_api_key'], 403),
        (assets['user_role_api_key'], 403),
        (assets['banned_role_api_key'], 403),
        (assets['bad_api_key'], 401),
        (None, 422),
    ],
)
def test_access_get_tokens(user, token, user_role_token, worker_role_token, banned_role_token, api_key, status_code):
    """
    Test access to the get tokens endpoint.
    """
    request = deepcopy(assets['create_token_example'])
    request['user_id'] = user.id
    response = internal_client.post('/v0.1/tokens', json=request)
    assert response.status_code == 200

    response = internal_client.get('/v0.1/tokens/{}'.format('?api_key={}'.format(api_key) if api_key else ''))
    assert response.status_code == status_code


@pytest.mark.parametrize(
    'api_key, status_code',
    [
        (assets['api_key'], 200),
        (assets['worker_role_api_key'], 403),
        (assets['user_role_api_key'], 403),
        (assets['banned_role_api_key'], 403),
        (assets['bad_api_key'], 401),
        (None, 422),
    ],
)
def test_access_update_token(user, token, user_role_token, worker_role_token, banned_role_token, api_key, status_code):
    """
    Test access to the update users endpoint.
    """
    request = deepcopy(assets['create_token_example'])
    request['user_id'] = user.id
    response = internal_client.post('/v0.1/tokens', json=request)
    assert response.status_code == 200
    token_id = json.loads(response.content)['id']

    request = deepcopy(assets['update_token_example'])
    request['api_key'] = api_key
    request['user_id'] = user.id

    response = internal_client.put('/v0.1/tokens/{}'.format(token_id), json=request)
    assert response.status_code == status_code


@pytest.mark.parametrize(
    'api_key, status_code',
    [
        (assets['api_key'], 200),
        (assets['worker_role_api_key'], 403),
        (assets['user_role_api_key'], 403),
        (assets['banned_role_api_key'], 403),
        (assets['bad_api_key'], 401),
        (None, 422),
    ],
)
def test_access_delete_token(user, token, user_role_token, worker_role_token, banned_role_token, api_key, status_code):
    """
    Test access to the delete token endpoint.
    """
    request = deepcopy(assets['create_token_example'])
    request['user_id'] = user.id
    response = internal_client.post('/v0.1/tokens', json=request)
    token_id = json.loads(response.content)['id']
    assert response.status_code == 200

    response = internal_client.delete(
        '/v0.1/tokens/{}{}'.format(token_id, '?api_key={}'.format(api_key) if api_key else '')
    )
    assert response.status_code == status_code
