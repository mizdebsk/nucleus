import json

import pkg_resources
import pytest
from fastapi.testclient import TestClient

from tft.nucleus.api.public import api

from .conftest import public_client


def test_version():
    """
    Test the version endpoint.
    """
    response = public_client.get("/v0.1/about")
    expected_app_version = pkg_resources.get_distribution("tft-api").version

    about = json.loads(response.content)

    assert response.status_code == 200
    assert about['app_version'] == expected_app_version


def test_404():
    """
    Test non-existing endpoint.
    """
    response = public_client.get("/v0.1/unknown-endpoint")

    assert response.status_code == 404


def test_redirection():
    """
    Test redirection from root endpoint to docs.
    """
    response = public_client.get("/", allow_redirects=False)
    assert response.status_code == 307
    assert response.headers['Location'] == '/redoc'
