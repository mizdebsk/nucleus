import time
from uuid import UUID, uuid4

import pytest
import sqlalchemy.exc
from fastapi.testclient import TestClient
from ruamel.yaml import YAML
from sqlalchemy_utils import create_database, database_exists

from alembic import command
from alembic.config import Config
from tft.nucleus.api.core import errors
from tft.nucleus.api.core.database import (
    Request,
    SessionLocal,
    Token,
    User,
    engine,
    get_user_by_auth_id_and_auth_method,
)
from tft.nucleus.api.internal import api as internal_api
from tft.nucleus.api.public import api as public_api

public_client = TestClient(public_api)
internal_client = TestClient(internal_api)

# Path to assets relative to tox location
ASSERTS_PATH = "tests/assets.yml"


def get_assets():
    yaml = YAML(typ='safe')
    with open(ASSERTS_PATH, 'r') as assets_file:
        assets = yaml.load(assets_file)
    return assets


assets = get_assets()


@pytest.fixture(scope="session", autouse=True)
def create_nucleus_database() -> None:
    print(f'Using database {str(engine.url)}')
    if not database_exists(engine.url):
        create_database(engine.url)
    alembic_cfg = Config('alembic.ini')
    alembic_cfg.set_main_option('sqlalchemy.url', str(engine.url))
    # Sometimes when tests run in parallel we hit this problem:
    # https://www.cockroachlabs.com/docs/v21.1/transaction-retry-error-reference#retry_serializable
    # Add a retry with a small sleep to workaround.
    try:
        command.upgrade(alembic_cfg, 'head')
    except sqlalchemy.exc.OperationalError:
        print(f'Retrying alembic upgrade')
        time.sleep(1)
        command.upgrade(alembic_cfg, 'head')


# We need to have at least one token and user to use the API
@pytest.fixture
def user() -> User:
    return add_user(auth_id='developer', auth_method='login')


@pytest.fixture
def token(user) -> Token:
    return add_token(user_id=user.id, name='test-name', api_key='test-key', enabled=True, ranch='public', role='admin')


@pytest.fixture
def user_role_token(user) -> Token:
    return add_token(
        user_id=user.id,
        name='user-role',
        api_key=assets['user_role_api_key'],
        enabled=True,
        ranch='public',
        role='user',
    )


@pytest.fixture
def worker_role_token(user) -> Token:
    return add_token(
        user_id=user.id,
        name='worker-role',
        api_key=assets['worker_role_api_key'],
        enabled=True,
        ranch='public',
        role='worker',
    )


@pytest.fixture
def banned_role_token(user) -> Token:
    return add_token(
        user_id=user.id,
        name='banned-role',
        api_key=assets['banned_role_api_key'],
        enabled=True,
        ranch='public',
        role='banned',
    )


def add_user(auth_id: str, auth_method: str) -> User:
    session = SessionLocal()
    try:
        user = get_user_by_auth_id_and_auth_method(session, auth_id, auth_method)
    except errors.NoSuchEntityError:
        user = User(id=str(uuid4()), auth_id=auth_id, auth_method=auth_method, enabled=True)
        session.add(user)
        session.commit()
        session.refresh(user)
    return user


def add_token(user_id: str, name: str, api_key: str, enabled: bool, ranch: str, role: str) -> Token:
    session = SessionLocal()
    token = Token(id=str(uuid4()), user_id=user_id, name=name, api_key=api_key, enabled=enabled, ranch=ranch, role=role)
    session.add(token)
    session.commit()
    session.refresh(token)
    session.close()
    return token


def add_request(request: Request) -> None:
    session = SessionLocal()
    session.add(request)
    session.commit()
    session.refresh(request)
    session.close()


@pytest.fixture
def delete_all_requests():
    session = SessionLocal()
    session.query(Request).delete()
    session.commit()
    session.close()


@pytest.fixture
def delete_all_tokens(delete_all_requests):
    session = SessionLocal()
    session.query(Token).delete()
    session.commit()
    session.close()
