import json
from copy import deepcopy

import pkg_resources
import pytest

from tft.nucleus.api.public import api

from .conftest import assets, internal_client


def test_create_token(user, token):
    """
    Test the create token endpoint.
    """
    request = deepcopy(assets['create_token_example'])
    request['user_id'] = user.id
    response = internal_client.post('/v0.1/tokens', json=request)
    content = json.loads(response.content)

    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    expected_response = assets['create_token_response_example'].copy()
    expected_response['id'] = content['id']
    expected_response['created'] = content['created']
    expected_response['updated'] = content['updated']
    expected_response['user_id'] = content['user_id']
    assert content == expected_response


def test_get_token(user, token):
    """
    Test the get token endpoint.
    """
    request = deepcopy(assets['create_token_example'])
    request['user_id'] = user.id
    response = internal_client.post('/v0.1/tokens', json=request)
    create_response_content = json.loads(response.content)

    assert response.status_code == 200

    response = internal_client.get(
        '/v0.1/tokens/{}?api_key={}'.format(create_response_content['id'], assets['api_key'])
    )
    content = json.loads(response.content)

    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    expected_response = assets['create_token_response_example'].copy()
    expected_response['id'] = create_response_content['id']
    expected_response['created'] = create_response_content['created']
    expected_response['updated'] = create_response_content['updated']
    expected_response['user_id'] = content['user_id']
    assert content == expected_response


def test_get_tokens(user, delete_all_tokens, token):
    """
    Test the get tokens endpoint.
    """
    expected_responses = []

    # First of all, get expected response for already created token
    # and in meanwhile test get_tokens with only one token
    response = internal_client.get('/v0.1/tokens/?api_key={}'.format(assets['api_key']))
    content = json.loads(response.content)

    assert response.status_code == 200

    expected_responses.append(content[0])

    for _ in range(3):
        # Create 3 tokens, one is already created by the fixture
        request = deepcopy(assets['create_token_example'])
        request['user_id'] = user.id
        response = internal_client.post('/v0.1/tokens', json=request)
        content = json.loads(response.content)

        assert response.status_code == 200

        expected_response = assets['create_token_response_example'].copy()
        expected_response['id'] = content['id']
        expected_response['created'] = content['created']
        expected_response['updated'] = content['updated']
        expected_response['user_id'] = content['user_id']
        expected_responses.append(expected_response)
        assert content == expected_response

    response = internal_client.get('/v0.1/tokens?api_key={}'.format(assets['api_key']))
    content = json.loads(response.content)

    assert response.status_code == 200

    assert sorted(content, key=lambda x: x['id']) == sorted(expected_responses, key=lambda x: x['id'])


def test_update_token(user, token):
    """
    Test the update token endpoint.
    """
    request = deepcopy(assets['create_token_example'])
    request['user_id'] = user.id
    response = internal_client.post('/v0.1/tokens', json=request)
    create_response_content = json.loads(response.content)

    assert response.status_code == 200

    request = deepcopy(assets['update_token_example'])
    request['user_id'] = user.id
    response = internal_client.put('/v0.1/tokens/{}'.format(create_response_content['id']), json=request)
    content = json.loads(response.content)

    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    expected_response = assets['update_token_response_example'].copy()
    expected_response['id'] = content['id']
    expected_response['created'] = content['created']
    expected_response['updated'] = content['updated']
    expected_response['user_id'] = content['user_id']
    assert content == expected_response


def test_delete_token(user, token):
    """
    Test the delete token endpoint.
    """
    request = deepcopy(assets['create_token_example'])
    request['user_id'] = user.id
    response = internal_client.post('/v0.1/tokens', json=request)
    create_response_content = json.loads(response.content)

    assert response.status_code == 200

    response = internal_client.delete(
        '/v0.1/tokens/{}?api_key={}'.format(create_response_content['id'], assets['api_key'])
    )

    assert response.status_code == 200

    response = internal_client.get(
        '/v0.1/tokens/{}?api_key={}'.format(create_response_content['id'], assets['api_key'])
    )
    content = json.loads(response.content)

    assert response.status_code == 404

    assert content['message'] == 'No such entity'
