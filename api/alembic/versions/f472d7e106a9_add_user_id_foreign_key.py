"""add user_id foreign key

Revision ID: f472d7e106a9
Revises: 19db7bd96405
Create Date: 2024-06-07 15:33:20.368186

"""
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID

from alembic import op

# revision identifiers, used by Alembic.
revision = 'f472d7e106a9'
down_revision = '19db7bd96405'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('tokens', 'user_id', existing_type=UUID(), nullable=False)
    op.create_foreign_key('fk_user_id_ref_users', 'tokens', 'users', ['user_id'], ['id'])


def downgrade():
    op.alter_column('tokens', 'user_id', existing_type=UUID(), nullable=True)
    op.drop_constraint('fk_user_id_ref_users', 'tokens', type_='foreignkey')
