"""empty message

Revision ID: 320b4191c1f4
Revises: db70a687a0fa
Create Date: 2024-02-09 14:53:05.576108

"""
import sqlalchemy as sa

from alembic import op
from tft.nucleus.api.config import settings

# revision identifiers, used by Alembic.
revision = '320b4191c1f4'
down_revision = 'db70a687a0fa'
branch_labels = None
depends_on = None


def update_user_role(user_id_list, role):
    connection = op.get_bind()

    for user_id in user_id_list:
        result = connection.execute(f"SELECT * FROM users WHERE id = '{user_id}'").fetchall()
        if not result:
            raise Exception(f"User with id {user_id} not found")

        op.execute(f"UPDATE users SET role = '{role}' WHERE id = '{user_id}'")


def upgrade():
    admin_users_id = settings.get('TESTING_FARM_ADMINS', [])
    update_user_role(admin_users_id, 'admin')

    worker_users_id = settings.get('TESTING_FARM_WORKERS', [])
    update_user_role(worker_users_id, 'worker')


def downgrade():
    admin_users_id = settings.get('TESTING_FARM_ADMINS', [])
    update_user_role(admin_users_id, 'user')

    worker_users_id = settings.get('TESTING_FARM_WORKERS', [])
    update_user_role(worker_users_id, 'user')
