"""rename user to token

Revision ID: becad9b43a0e
Revises: 320b4191c1f4
Create Date: 2024-05-31 11:57:37.929313

"""
import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = 'becad9b43a0e'
down_revision = '320b4191c1f4'
branch_labels = None
depends_on = None


def upgrade():
    op.rename_table('users', 'tokens')
    op.alter_column(
        'requests',
        'user_id',
        new_column_name='token_id',
    )

    op.drop_constraint('fk_user_id_ref_users', 'requests', type_='foreignkey')
    op.create_foreign_key('fk_token_id_ref_tokens', 'requests', 'tokens', ['token_id'], ['id'])


def downgrade():
    op.rename_table('tokens', 'users')
    op.alter_column(
        'requests',
        'token_id',
        new_column_name='user_id',
    )

    op.drop_constraint('fk_token_id_ref_tokens', 'requests', type_='foreignkey')
    op.create_foreign_key('fk_user_id_ref_users', 'requests', 'users', ['user_id'], ['id'])
