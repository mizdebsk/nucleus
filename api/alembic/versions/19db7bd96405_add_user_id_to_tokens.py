"""add user_id to tokens

Revision ID: 19db7bd96405
Revises: e2a41c85752c
Create Date: 2024-06-07 14:58:48.792496

"""
import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = '19db7bd96405'
down_revision = 'e2a41c85752c'
branch_labels = None
depends_on = None


def upgrade():
    connection = op.get_bind()
    result = connection.execute(
        sa.text("SELECT id FROM users WHERE users.auth_id = 'default' AND users.auth_method = 'default';")
    )
    user_id = str(result.first()[0])
    op.execute(f"UPDATE tokens SET user_id='{user_id}' WHERE tokens.user_id IS NULL;")


def downgrade():
    pass
