"""add user table

Revision ID: e2a41c85752c
Revises: becad9b43a0e
Create Date: 2024-06-03 14:18:31.237644

"""
import datetime
from uuid import uuid4

import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

from alembic import op
from tft.nucleus.api.core.database import User

# revision identifiers, used by Alembic.
revision = 'e2a41c85752c'
down_revision = 'becad9b43a0e'
branch_labels = None
depends_on = None


def upgrade():
    users_table = op.create_table(
        'users',
        sa.Column('created', sa.DateTime(), nullable=False),
        sa.Column('updated', sa.DateTime(), nullable=False),
        sa.Column('id', postgresql.UUID(), nullable=False),
        sa.Column('auth_id', sa.String(), nullable=False),
        sa.Column('auth_method', sa.String(), nullable=False),
        sa.Column('enabled', sa.Boolean(), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('auth_id', 'auth_method'),
    )

    op.bulk_insert(
        users_table,
        [
            {
                'created': datetime.datetime.utcnow(),
                'updated': datetime.datetime.utcnow(),
                'id': str(uuid4()),
                'auth_id': 'default',
                'auth_method': 'default',
                'enabled': True,
            }
        ],
    )

    op.add_column('tokens', sa.Column('user_id', postgresql.UUID(), nullable=True))


def downgrade():
    op.drop_column('tokens', 'user_id')
    op.drop_table('users')
