# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides the implementation of compose Create, Read, Update, and Delete database operations.
"""
from functools import lru_cache
from typing import List

from ..config import settings
from ..core.schemes.compose import Compose, SupportedComposesOut


@lru_cache
def _get_composes_from_file(file: str) -> List[Compose]:
    with open(file, 'r', encoding='utf8') as composes_file:
        composes = composes_file.read().splitlines()

        composes_list = []
        for compose in composes:
            # Ignore empty lines
            if len(compose) > 0:
                composes_list.append(Compose(name=compose))
    return composes_list


def get_ranch_composes(ranch: str) -> SupportedComposesOut:
    """
    Return supported composes in a ranch
    """
    composes_out = _get_composes_from_file(settings.COMPOSES[ranch.upper()])
    return SupportedComposesOut(composes=composes_out)


def get_composes() -> SupportedComposesOut:
    """
    Return supported composes in the public ranch
    """
    return get_ranch_composes('public')
