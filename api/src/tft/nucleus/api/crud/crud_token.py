# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of tokens Create, Read, Update, and Delete database operations.
"""
from typing import List
from uuid import UUID, uuid4

from sqlalchemy.orm import Session

from ..core import errors
from ..core.access_control import AccessAction, AccessObject, check_access_api_key
from ..core.database import (
    DEFAULT_TOKEN_ROLE,
    Token,
    get_token_by_api_key,
    get_token_by_token_id,
    get_user_by_id,
)
from ..core.schemes import token


def create_token(session: Session, token_in: token.TokenCreateUpdateIn) -> token.TokenCreateGetUpdateOut:
    """
    Create a token in database and transform it to response schema.
    """
    check_access_api_key(session, token_in.api_key, AccessObject.USER, AccessAction.CREATE)

    token_id = str(uuid4())

    try:
        UUID(token_in.user_id, version=4)
    except ValueError:
        raise errors.BadRequestError(message="Invalid user_id.")  # pylint: disable=raise-missing-from

    try:
        get_user_by_id(session, token_in.user_id)
    except errors.NoSuchEntityError:
        raise errors.BadRequestError(  # pylint: disable=raise-missing-from
            message="User with provided user_id not found."
        )

    token_db = Token(
        id=token_id,
        user_id=token_in.user_id,
        name=token_in.name,
        api_key=token_in.token_api_key,
        enabled=token_in.enabled,
        role=token_in.role or DEFAULT_TOKEN_ROLE,
    )
    session.add(token_db)
    session.commit()
    session.refresh(token_db)
    token_out = token.TokenCreateGetUpdateOut(
        id=token_db.id,
        user_id=token_db.user_id,
        name=token_db.name,
        api_key=token_db.api_key,
        enabled=token_db.enabled,
        role=token_db.role,
        created=token_db.created,
        updated=token_db.updated,
    )
    return token_out


def get_token(session: Session, token_id: UUID, api_key: str) -> token.TokenCreateGetUpdateOut:
    """
    Retrieve token from database and transform to response schema.
    """
    requesting_token = get_token_by_api_key(session, api_key)

    if requesting_token.id == str(token_id):
        check_access_api_key(session, api_key, AccessObject.USER_SELF, AccessAction.GET)
    else:
        check_access_api_key(session, api_key, AccessObject.USER, AccessAction.GET)

    token_db = get_token_by_token_id(session, token_id)

    token_out = token.TokenCreateGetUpdateOut(
        id=token_db.id,
        user_id=token_db.user_id,
        name=token_db.name,
        api_key=token_db.api_key,
        enabled=token_db.enabled,
        role=token_db.role,
        created=token_db.created,
        updated=token_db.updated,
    )
    return token_out


def get_tokens(session: Session, api_key: str) -> List[token.TokenCreateGetUpdateOut]:
    """
    Retrieve tokens from database and transform to response schemes list.
    """
    check_access_api_key(session, api_key, AccessObject.USERS_LIST, AccessAction.GET)

    tokens_db = session.query(Token).all()
    if not tokens_db:
        raise errors.NoSuchEntityError()

    tokens_out = []
    for token_db in tokens_db:
        tokens_out.append(
            token.TokenCreateGetUpdateOut(
                id=token_db.id,
                user_id=token_db.user_id,
                name=token_db.name,
                api_key=token_db.api_key,
                enabled=token_db.enabled,
                role=token_db.role,
                created=token_db.created,
                updated=token_db.updated,
            )
        )
    return tokens_out


def update_token(
    session: Session, token_id: UUID, token_in: token.TokenCreateUpdateIn
) -> token.TokenCreateGetUpdateOut:
    """
    Update token in database and return response schema.
    """
    check_access_api_key(session, token_in.api_key, AccessObject.USER, AccessAction.UPDATE)

    update_token_db = get_token_by_token_id(session, token_id)

    # For enabled flag we can't just do `token_in.enabled or update_token_db.enabled` (False or True will return False)
    if token_in.enabled is not None:
        enabled = token_in.enabled
    else:
        enabled = update_token_db.enabled

    updated_token_db = (
        session.query(Token)
        .filter(Token.id == str(token_id))
        .update(
            {
                'name': token_in.name or update_token_db.name,
                'api_key': token_in.token_api_key or update_token_db.api_key,
                'enabled': enabled,
                'role': token_in.role or update_token_db.role,
            }
        )
    )

    if not updated_token_db:
        raise errors.NoSuchEntityError()

    session.commit()

    token_db = session.query(Token).filter(Token.id == str(token_id)).first()
    assert token_db is not None

    token_out = token.TokenCreateGetUpdateOut(
        id=token_db.id,
        user_id=token_db.user_id,
        name=token_db.name,
        api_key=token_db.api_key,
        enabled=token_db.enabled,
        role=token_db.role,
        created=token_db.created,
        updated=token_db.updated,
    )
    return token_out


def delete_token(session: Session, token_id: UUID, api_key: str) -> None:
    """
    Delete the token from database.
    """
    check_access_api_key(session, api_key, AccessObject.USER, AccessAction.DELETE)

    token_db = get_token_by_token_id(session, token_id)

    session.delete(token_db)  # type: ignore
    session.commit()
