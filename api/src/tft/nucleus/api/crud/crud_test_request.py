# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of test requests Create, Read, Update, and Delete database operations.
"""
import re
from datetime import datetime
from typing import Dict, List, Optional, Tuple
from uuid import UUID, uuid4

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from ..config import settings
from ..core import errors, helpers
from ..core.access_control import (
    AccessAction,
    AccessObject,
    check_access_api_key,
    is_test_request_owned_by_token,
)
from ..core.database import (
    Request,
    RequestStateType,
    get_test_request_by_id,
    get_token_by_api_key,
)
from ..core.schemes import test_request
from .crud_composes import _get_composes_from_file

STATES_EXCEPT_CANCELED: List[RequestStateType] = [
    state for state in RequestStateType if state != RequestStateType.CANCELED
]

# Allowed state transitions from current state to future states
ALLOWED_STATE_TRANSITIONS: Dict[RequestStateType, List[RequestStateType]] = {
    RequestStateType.NEW: STATES_EXCEPT_CANCELED,
    RequestStateType.QUEUED: STATES_EXCEPT_CANCELED,
    RequestStateType.RUNNING: STATES_EXCEPT_CANCELED,
    RequestStateType.COMPLETE: STATES_EXCEPT_CANCELED,
    RequestStateType.ERROR: STATES_EXCEPT_CANCELED,
    RequestStateType.CANCEL_REQUESTED: [
        RequestStateType.CANCEL_REQUESTED,
        RequestStateType.CANCELED,
    ],
    RequestStateType.CANCELED: [
        RequestStateType.CANCELED,
    ],
}


def _request_to_request_get_update_out(request: Request) -> test_request.RequestGetUpdateOut:
    """
    Transform test request database model to RequestGetUpdateOut response schema.
    """
    request_out = test_request.RequestGetUpdateOut(
        id=request.id,
        # NOTE: user_id is the same as token_id, it is preserved for backwards compatibility
        user_id=request.token_id,
        token_id=request.token_id,
        test=request.test,
        # TODO: change once database migrated to enums with string values
        state=str(request.state),
        environments_requested=request.environments_requested,
        notes=request.notes,
        result=request.result,
        run=request.run,
        settings=request.settings,
        user=test_request.RequestUser(webpage=None),
        queued_time=request.queued_time,
        run_time=request.run_time,
        created=request.created,
        updated=request.updated,
    )
    if request.user_webpage_url and request_out.user:
        request_out.user.webpage = test_request.RequestUserWebpage(
            url=request.user_webpage_url,
            icon=request.user_webpage_icon,
            name=request.user_webpage_name,
        )
    return request_out


def _get_test_request(session: Session, request_id: UUID) -> test_request.RequestGetUpdateOut:
    request = session.query(Request).filter(Request.id == str(request_id)).first()
    if not request:
        raise errors.NoSuchEntityError()
    return _request_to_request_get_update_out(request)


def _get_test_request_authenticated(session: Session, request_id: UUID) -> test_request.RequestGetAuthenticatedOut:
    request = session.query(Request).filter(Request.id == str(request_id)).first()
    if not request:
        raise errors.NoSuchEntityError()
    request_out = test_request.RequestGetAuthenticatedOut(
        id=request.id,
        # NOTE: user_id is the same as token_id, it is preserved for backwards compatibility
        token_id=request.token_id,
        user_id=request.token_id,
        test=request.test,
        # TODO: change once database migrated to enums with string values
        state=str(request.state),
        environments_requested=request.environments_requested,
        notes=request.notes,
        result=request.result,
        run=request.run,
        settings=request.settings,
        user=test_request.RequestUser(webpage=None),
        queued_time=request.queued_time,
        run_time=request.run_time,
        created=request.created,
        updated=request.updated,
        notification=request.notification,
    )
    if request.user_webpage_url and request_out.user:
        request_out.user.webpage = test_request.RequestUserWebpage(
            url=request.user_webpage_url,
            icon=request.user_webpage_icon,
            name=request.user_webpage_name,
        )
    return request_out


def _remove_secrets(request: test_request.RequestGetUpdateOut) -> test_request.RequestGetUpdateOut:
    # hide possible sensitive tmt environment variables
    if request.environments_requested:
        for environment in request.environments_requested:
            environment.secrets = None
            if environment.tmt:
                environment.tmt.environment = None

    # git url can contain secrets
    if request.test:
        test = request.test.fmf or request.test.sti
        # test must exist
        assert test is not None
        test.url = helpers.hide_cred_in_url(test.url)

    return request


def get_test_request(session: Session, request_id: UUID) -> test_request.RequestGetUpdateOut:
    """
    Retrieve request from database and transform to response schema.
    """
    check_access_api_key(session, None, AccessObject.REQUEST, AccessAction.GET)

    request_out = _get_test_request(session, request_id)
    return _remove_secrets(request_out)


def get_test_requests(
    session: Session,
    state: Optional[str],
    token_id: Optional[UUID],
    created_before: Optional[str],
    created_after: Optional[str],
) -> Optional[List[test_request.RequestGetUpdateOut]]:
    """
    Get all test requests with the given state
    """
    check_access_api_key(session, None, AccessObject.REQUESTS_LIST, AccessAction.GET)

    if not any([state, token_id]):
        return None

    if state and state not in [str(state) for state in RequestStateType]:
        return None

    requests_query = session.query(Request)

    if state:
        requests_query = requests_query.filter(Request.state == RequestStateType[state.upper()].value)

    if token_id:
        requests_query = requests_query.filter(Request.token_id == str(token_id))

    if created_before:
        requests_query = requests_query.filter(Request.created < datetime.fromisoformat(created_before))

    if created_after:
        requests_query = requests_query.filter(Request.created > datetime.fromisoformat(created_after))

    requests = requests_query.all()

    if not requests:
        return None

    requests_out = []
    for request in requests:
        request_out = _request_to_request_get_update_out(request)
        requests_out.append(_remove_secrets(request_out))

    return requests_out


def get_test_request_authenticated(
    session: Session, request_id: UUID, api_key: str
) -> test_request.RequestGetAuthenticatedOut:
    """
    Authenticate and retrieve request from database and transform to response schema.
    """
    if is_test_request_owned_by_token(session, api_key, request_id):
        check_access_api_key(session, api_key, AccessObject.REQUEST_OWNED, AccessAction.GET_SECRETS)

    else:
        check_access_api_key(session, api_key, AccessObject.REQUEST, AccessAction.GET_SECRETS)

    return _get_test_request_authenticated(session, request_id)


def create_test_request(
    session: Session, test_request_create_in: test_request.RequestCreateIn
) -> test_request.RequestCreateOut:
    """
    Create test request in database and transform it to response schema.
    """
    check_access_api_key(session, test_request_create_in.api_key, AccessObject.REQUEST, AccessAction.CREATE)
    auth_token = get_token_by_api_key(session, test_request_create_in.api_key)

    # If tmt alias is used, move test to fmf
    if hasattr(test_request_create_in.test, "tmt") and test_request_create_in.test.tmt is not None:
        test_request_create_in.test.fmf = test_request_create_in.test.tmt
        test_request_create_in.test.tmt = None

    if auth_token.ranch is None:
        raise errors.GenericError("The token's ranch is not defined")
    composes = _get_composes_from_file(settings.COMPOSES[auth_token.ranch.upper()])

    if test_request_create_in.environments is not None:
        for environment in test_request_create_in.environments:

            if environment.os is not None:
                if not any(re.match(compose.name, environment.os.compose) for compose in composes):

                    raise errors.BadRequestError(
                        message=f"Compose {environment.os.compose} does not exist. "
                        + f"Go to {settings.COMPOSE_DOCS} to find all available composes."
                    )

    if (
        test_request_create_in.test.sti
        and test_request_create_in.settings
        and test_request_create_in.settings.pipeline
        and test_request_create_in.settings.pipeline.type == test_request.SettingsPipelineNames.TMT_MULTIHOST
    ):
        raise errors.BadRequestError(
            message=f'The value `{test_request.SettingsPipelineNames.TMT_MULTIHOST}` of option `settings.pipeline.type`'
            ' is incompatible with STI test type.'
        )

    request_id = str(uuid4())
    db_test_request = Request(
        id=request_id,
        token_id=auth_token.id,
        test=jsonable_encoder(test_request_create_in.test),
        environments_requested=jsonable_encoder(test_request_create_in.environments),
        notification=jsonable_encoder(test_request_create_in.notification),
        settings=jsonable_encoder(test_request_create_in.settings),
    )
    if test_request_create_in.user and test_request_create_in.user.webpage:
        db_test_request.user_webpage_url = test_request_create_in.user.webpage.url
        db_test_request.user_webpage_icon = test_request_create_in.user.webpage.icon
        db_test_request.user_webpage_name = test_request_create_in.user.webpage.name

    session.add(db_test_request)
    session.commit()
    session.refresh(db_test_request)
    request_out = test_request.RequestCreateOut(
        id=request_id,
        test=test_request_create_in.test,
        notification=test_request_create_in.notification,
        environments=test_request_create_in.environments,
        settings=test_request_create_in.settings,
        user=test_request_create_in.user,
        created=db_test_request.created,
        updated=db_test_request.updated,
        # TODO: change once database migrated to enums with string values
        # state=db_test_request.state,
        state=str(db_test_request.state),
    )
    return request_out


def update_test_request(  # pylint: disable=too-many-locals
    session: Session, request_id: UUID, test_request_in: test_request.RequestUpdateIn
) -> test_request.RequestGetUpdateOut:
    """
    Update test request in database and return response schema.
    """
    if is_test_request_owned_by_token(session, test_request_in.api_key, request_id):
        check_access_api_key(session, test_request_in.api_key, AccessObject.REQUEST_OWNED, AccessAction.UPDATE)

    else:
        check_access_api_key(session, test_request_in.api_key, AccessObject.REQUEST, AccessAction.UPDATE)

    update_test_request_db = get_test_request_by_id(session, request_id)

    queued_time = None
    if test_request_in.state == str(RequestStateType.RUNNING):
        queued_time = datetime.utcnow() - update_test_request_db.created

    run_time = None
    if test_request_in.state in [
        str(RequestStateType.COMPLETE),
        str(RequestStateType.ERROR),
        str(RequestStateType.CANCELED),
    ]:
        run_time = datetime.utcnow() - update_test_request_db.created

    state = update_test_request_db.state

    # TODO: change getting state once database migrated to enums with string values
    if test_request_in.state:

        current_state = update_test_request_db.state
        future_state = RequestStateType.from_string(test_request_in.state)

        if future_state not in ALLOWED_STATE_TRANSITIONS[current_state]:
            raise errors.ConflictError(
                # TODO: find better way to map states
                message=f"Request in state '{str(current_state)}' cannot be changed to '{str(future_state)}'."
            )

        state = future_state

    notes = update_test_request_db.notes
    if test_request_in.notes:
        notes = jsonable_encoder(test_request_in.notes)

    run = update_test_request_db.run
    if test_request_in.run:
        run = jsonable_encoder(test_request_in.run)

    result = update_test_request_db.result
    if test_request_in.result:
        result = jsonable_encoder(test_request_in.result)

    updated_test_request_db = (
        session.query(Request)
        .filter(Request.id == str(request_id))
        .update(
            {
                'state': state,
                'notes': notes,
                'environments_requested': test_request_in.environments_requested
                or update_test_request_db.environments_requested,
                'result': result,
                'run': run,
                'notification': test_request_in.notification or update_test_request_db.notification,
                'queued_time': queued_time or update_test_request_db.queued_time,
                'run_time': run_time or update_test_request_db.run_time,
            }
        )
    )

    if not updated_test_request_db:
        raise errors.NoSuchEntityError()

    session.commit()

    test_request_db = session.query(Request).filter(Request.id == str(request_id)).first()
    assert test_request_db is not None

    test_request_out = _request_to_request_get_update_out(test_request_db)
    return test_request_out


def delete_test_request(
    session: Session, request_id: UUID, test_request_delete_in: test_request.RequestDeleteIn
) -> Tuple[int, test_request.RequestGetUpdateOut]:
    """
    Cancel test request in database and return response schema.
    """
    if is_test_request_owned_by_token(session, test_request_delete_in.api_key, request_id):
        check_access_api_key(session, test_request_delete_in.api_key, AccessObject.REQUEST_OWNED, AccessAction.DELETE)

    else:
        check_access_api_key(session, test_request_delete_in.api_key, AccessObject.REQUEST, AccessAction.DELETE)

    update_test_request_db = get_test_request_by_id(session, request_id)

    state = update_test_request_db.state

    if state in [RequestStateType.COMPLETE, RequestStateType.ERROR]:
        raise errors.ConflictError(
            # TODO: find better way to map states
            message=f"Request in state '{str(state)}' cannot be canceled"
        )

    status_code = 200

    if state in [RequestStateType.CANCEL_REQUESTED, RequestStateType.CANCELED]:
        status_code = 204

    else:
        session.query(Request).filter(Request.id == str(request_id)).update(
            {'state': RequestStateType.CANCEL_REQUESTED}
        )
        session.commit()

    request_out = _get_test_request(session, request_id)
    return status_code, _remove_secrets(request_out)
