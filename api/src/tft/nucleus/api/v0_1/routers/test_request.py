# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of test requests router
"""
from typing import List, Optional
from uuid import UUID

from fastapi import APIRouter, Depends, Response
from sqlalchemy.orm import Session

from ...core.database import get_db
from ...core.schemes import test_request
from ...crud import crud_test_request

internal_router = APIRouter()
public_router = APIRouter()


@internal_router.post('', response_model=test_request.RequestCreateOut)
@public_router.post('', response_model=test_request.RequestCreateOut)
def request_a_new_test(
    request_in: test_request.RequestCreateIn, session: Session = Depends(get_db)
) -> test_request.RequestCreateOut:
    """
    Create new test request handler
    """
    return crud_test_request.create_test_request(session, request_in)


@public_router.get('/{request_id}', response_model=test_request.RequestGetUpdateOut)
def test_request_details(request_id: str, session: Session = Depends(get_db)) -> test_request.RequestGetUpdateOut:
    """
    Get test request handler
    """
    return crud_test_request.get_test_request(session, request_id)  # type: ignore


@public_router.get('', response_model=Optional[List[test_request.RequestGetUpdateOut]])
def get_test_requests(
    state: Optional[str] = None,
    # NOTE: user_id is the same as token_id, it is preserved for backwards compatibility
    token_id: Optional[UUID] = None,
    user_id: Optional[UUID] = None,
    created_before: Optional[str] = None,
    created_after: Optional[str] = None,
    session: Session = Depends(get_db),
) -> Optional[List[test_request.RequestGetUpdateOut]]:
    """
    Get all test requests with the given state handler
    """
    return crud_test_request.get_test_requests(session, state, token_id or user_id, created_before, created_after)


@internal_router.get('/{request_id}', response_model=test_request.RequestGetAuthenticatedOut)
def test_request_details_authenticated(
    request_id: str, api_key: str, session: Session = Depends(get_db)
) -> test_request.RequestGetUpdateOut:
    """
    Get test request handler
    """
    return crud_test_request.get_test_request_authenticated(session, request_id, api_key)  # type: ignore


@internal_router.put('/{request_id}', response_model=test_request.RequestGetUpdateOut)
def update_test_request(
    request_id: str, request_in: test_request.RequestUpdateIn, session: Session = Depends(get_db)
) -> test_request.RequestGetUpdateOut:
    """
    Update test request handler
    """
    return crud_test_request.update_test_request(session, UUID(request_id), request_in)


@public_router.delete('/{request_id}', response_model=test_request.RequestGetUpdateOut)
def delete_test_request(
    request_id: str, request_in: test_request.RequestDeleteIn, response: Response, session: Session = Depends(get_db)
) -> test_request.RequestGetUpdateOut:
    """
    Delete test request handler
    """
    response.status_code, request_update = crud_test_request.delete_test_request(session, UUID(request_id), request_in)

    return request_update
