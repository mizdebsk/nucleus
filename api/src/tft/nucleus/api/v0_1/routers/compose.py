# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of test requests router
"""
from fastapi import APIRouter

from ...core.schemes import compose
from ...crud.crud_composes import get_composes, get_ranch_composes

router = APIRouter()


@router.get('', response_model=compose.SupportedComposesOut)
def supported_composes() -> compose.SupportedComposesOut:
    """
    Return supported composes
    """
    return get_composes()


@router.get('/{ranch}', response_model=compose.SupportedComposesOut)
def supported_ranch_composes(ranch: str) -> compose.SupportedComposesOut:
    """
    Create new test request handler
    """
    return get_ranch_composes(ranch)
