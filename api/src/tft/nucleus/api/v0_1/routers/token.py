# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of tokens router
"""
from typing import List
from uuid import UUID

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from ...core.database import get_db
from ...core.schemes import token
from ...crud import crud_token

router = APIRouter()


@router.post('', response_model=token.TokenCreateGetUpdateOut)
def create_token(
    token_in: token.TokenCreateUpdateIn, session: Session = Depends(get_db)
) -> token.TokenCreateGetUpdateOut:
    """
    Create new token handler
    """
    return crud_token.create_token(session, token_in)


@router.put('/{token_id}', response_model=token.TokenCreateGetUpdateOut)
def update_token(
    token_id: str, token_in: token.TokenCreateUpdateIn, session: Session = Depends(get_db)
) -> token.TokenCreateGetUpdateOut:
    """
    Update new token handler
    """
    return crud_token.update_token(session, UUID(token_id), token_in)


@router.get('/{token_id}', response_model=token.TokenCreateGetUpdateOut)
def get_token(token_id: str, api_key: str, session: Session = Depends(get_db)) -> token.TokenCreateGetUpdateOut:
    """
    Get token handler
    """
    return crud_token.get_token(session, UUID(token_id), api_key)


@router.get('', response_model=List[token.TokenCreateGetUpdateOut])
def get_tokens(api_key: str, session: Session = Depends(get_db)) -> List[token.TokenCreateGetUpdateOut]:
    """
    Get tokens handler
    """
    return crud_token.get_tokens(session, api_key)


@router.delete('/{token_id}')
def delete_token(token_id: str, api_key: str, session: Session = Depends(get_db)) -> None:
    """
    Delete token handler
    """
    return crud_token.delete_token(session, UUID(token_id), api_key)
