# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides Sentry integration with API.
"""
import sentry_sdk

from .config import settings


def init_sentry() -> None:
    """
    Initialise Sentry integration.
    """
    if settings.SENTRY_ENABLED:
        sentry_sdk.init(
            dsn=settings.SENTRY_DSN,
            # Set traces_sample_rate to 1.0 to capture 100%
            # of transactions for performance monitoring.
            traces_sample_rate=settings.SENTRY_TRACES_SAMPLE_RATE,
            ca_certs=settings.get("SENTRY_CA_CERTS"),
            environment=settings.ENVIRONMENT,
        )
