# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides schemes of test requests
"""
import uuid
from datetime import datetime, timedelta
from enum import Enum
from typing import Any, Dict, List, Optional

from pydantic import (  # pylint: disable=no-name-in-module
    AnyUrl,
    Extra,
    Field,
    HttpUrl,
    root_validator,
    validator,
)

from ..errors import UnprocessableEntityError
from . import NucleusBaseModel


class ArtifactTypes(str, Enum):
    """
    Artifact type represents an artifact type which was built by a specific instance of a build system.
    """

    REPOSITORY_FILE = 'repository-file'
    REPOSITORY = 'repository'
    FEDORA_COPR_BUILD = 'fedora-copr-build'
    REDHAT_BREW_BUILD = 'redhat-brew-build'
    FEDORA_KOJI_BUILD = 'fedora-koji-build'


ARTIFACT_ORDER = {
    ArtifactTypes.REPOSITORY_FILE.value: 10,
    ArtifactTypes.REPOSITORY.value: 20,
    ArtifactTypes.FEDORA_COPR_BUILD.value: 30,
    ArtifactTypes.REDHAT_BREW_BUILD.value: 40,
    ArtifactTypes.FEDORA_KOJI_BUILD.value: 50,
}


class Architectures(str, Enum):
    """
    Force the given architecture of the test environment to provision.
    """

    X86_64 = 'x86_64'
    S390X = 's390x'
    AARCH64 = 'aarch64'
    PPC64LE = 'ppc64le'


class NoteLevels(str, Enum):
    """
    Level of the note.
    """

    INFO = 'info'
    WARNING = 'warning'
    ERROR = 'error'


class TestStageResults(str, Enum):
    """
    Result of the test stage.
    """

    PASSED = 'passed'
    FAILED = 'failed'
    SKIPPED = 'skipped'


class TestStates(str, Enum):
    """
    State of the request
    """

    NEW = 'new'
    QUEUED = 'queued'
    RUNNING = 'running'
    ERROR = 'error'
    COMPLETE = 'complete'
    CANCEL_REQUESTED = 'cancel-requested'
    CANCELED = 'canceled'


class TestOverallResults(str, Enum):
    """
    Overall result of testing the request. Special value `unknown` means that result could not be determined.
    The overall result `error` was added because some test frameworks recognize this state
    themselves, e.g. tmt.
    """

    PASSED = 'passed'
    FAILED = 'failed'
    SKIPPED = 'skipped'
    UNKNOWN = 'unknown'
    ERROR = 'error'


class SettingsPipelineNames(str, Enum):
    """
    Pipeline names which can be forced through the API.
    """

    TMT_MULTIHOST = 'tmt-multihost'


class TestFMFSettings(NucleusBaseModel):
    """
    Setting for FMF test
    """

    recognize_errors: bool = Field(
        False,
        alias='recognize-errors',
        description=(
            'By default we map `error` state from `tmt` to `failed` overall result of the request. '
            'If you want to recognize `error` outcome from tmt as an `error` overall state of the request, '
            'enable this setting. A real life usage of this option is in Fedora CI, where for generic tests '
            'it makes more sense to recognize these errors as they mean that the generic test is broken and it '
            'is the responsibility of Fedora CI to fix it. For tests defined by the users - i.e. generic tests, '
            'fixing these tests is the responsibility of the user and Fedora CI does not want them to mark them '
            'as `error`. Defaults to `false`.'
        ),
    )


class TestFMF(NucleusBaseModel):
    """
    Run all plans according to the TMT/FMF definition.
    Unique remote identifier of the FMF metadata as
    described in [documentation](https://fmf.readthedocs.io/en/latest/concept.html#identifiers).
    """

    url: AnyUrl = Field(
        ...,
        description=(
            'Git repository containing the metadata tree. Use any format acceptable by the git clone command.'
        ),
    )
    ref: Optional[str] = Field(
        'master',
        description=(
            'Branch, tag or commit specifying the desired git revision. '
            'This is used to perform a git checkout in the repository.'
        ),
    )
    merge_sha: Optional[str] = Field(
        None, description=('A target commit SHA to which the `ref` is merged to, if specified.')
    )
    path: Optional[str] = Field(
        '.',
        description=(
            'Path to the metadata tree root. '
            'Should be relative to the git repository root provided in the `url` parameter.'
        ),
    )
    name: Optional[str] = Field(
        None,
        description=(
            'Select plans to be executed. '
            'Passed as `--name` option to the `tmt plan` command. '
            'Can be a regular expression.'
        ),
    )
    settings: Optional[TestFMFSettings] = Field(
        None,
    )
    plan_filter: Optional[str] = Field(
        None,
        description=(
            'Filter tmt plans. '
            'Passed as `--filter` option to the `tmt plan` command. '
            'By default, `enabled:true` filter is applied. '
            'Plan filtering is similar to test filtering, '
            'see https://tmt.readthedocs.io/en/stable/examples.html#filter-tests for more information.'
        ),
    )
    test_name: Optional[str] = Field(
        None,
        description=(
            'Select tests to be executed. '
            'Passed as `--name` option to the `tmt test` command. '
            'Can be a regular expression.'
        ),
    )
    test_filter: Optional[str] = Field(
        None,
        description=(
            'Filter tmt tests. '
            'Passed as `--filter` option to the `tmt test` command. '
            'It overrides any test filter defined in the plan. '
            'See https://tmt.readthedocs.io/en/stable/examples.html#filter-tests for more information.'
        ),
    )


class TestSTI(NucleusBaseModel):
    """
    Run STI tests from the given GIT repository.
    """

    url: AnyUrl = Field(
        ...,
        description=('Git repository containing the STI tests. Use any format acceptable by the git clone command.'),
    )
    ref: str = Field(
        ...,
        description=(
            'Branch, tag or commit specifying the desired git revision. '
            'This is used to perform a git checkout in the repository.'
        ),
    )
    merge_sha: Optional[str] = Field(
        None, description=('A target commit SHA to which the `ref` is merged to, if specified.')
    )
    playbooks: Optional[List[str]] = Field(
        ['tests/tests*.yml'],
        description=(
            'Playbooks to run from the given repositories. Globbing is supported. '
            'By default standard `tests/tests*.yml` is used.'
        ),
    )
    extra_variables: Optional[List[Dict[str, str]]] = Field(
        None,
        description=(
            'A mapping of Ansible extra variable names to values, which will be passed to `ansible-playbook`.'
        ),
    )


class TestScript(NucleusBaseModel):
    """
    Run given scripts from the given GIT repository in the default shell.
    """

    url: AnyUrl = Field(
        ..., description=('Git repository containing the scripts. Use any format acceptable by the git clone command.')
    )
    ref: str = Field(
        ...,
        description=(
            'Branch, tag or commit specifying the desired git revision. '
            'This is used to perform a git checkout in the repository.'
        ),
    )
    script: List[str] = Field(
        ...,
        description=(
            'Scripts to run. Script is a command executed from the root of the cloned directory om the given '
            'test environment. More commands can be specified. Each command is asserted on return code 0.'
        ),
    )


class Test(NucleusBaseModel):
    """
    Details about the test to run. Only one test type can be specified.
    If the user needs to run multiple tests, it should do it in separate requests.
    """

    fmf: Optional[TestFMF] = Field(
        None,
    )
    script: Optional[TestScript] = Field(
        None,
    )
    sti: Optional[TestSTI] = Field(
        None,
    )

    class Config:
        """
        We should ignore extra field here because we have `tmt` alias for `fmf` test type.
        """

        extra = Extra.ignore


class TestCreateIn(NucleusBaseModel):
    """
    Details about the test to run. Only one test type can be specified.
    If the user needs to run multiple tests, it should do it in separate requests.
    The schema is used only for processing incoming test request
    which can have `tmt` test type.
    """

    fmf: Optional[TestFMF] = Field(
        None,
    )
    tmt: Optional[TestFMF] = Field(
        None,
    )
    script: Optional[TestScript] = Field(
        None,
    )
    sti: Optional[TestSTI] = Field(
        None,
    )


class Artifact(NucleusBaseModel):
    """
    Additional artifact to install in the test environment.
    """

    id: str = Field(
        ...,
        description=(
            'Unique identifier of the artifact. Value depends on the type of the artifact.\n\n'
            '* fedora-koji-build - use task ID of the koji build task, e.g. 43054146\n'
            '* redhat-brew-build - use brew task ID of the koji build task, e.g. 43054146\n'
            '* fedora-copr-build - use the copr build-id:chroot-name, e.g. 1784470:fedora-32-x86_64\n'
            '* repository - baseurl of an RPM repository to install packages from, e.g. https://my.repo/repository\n'
            '* repository-file - an URL to a repository file which should be added to /etc/yum.repos.d, '
            'e.g. https://example.cpore/repository.repo\n'
        ),
    )
    type: ArtifactTypes = Field(
        ...,
    )
    packages: Optional[List[str]] = Field(
        None,
        description=(
            'List of packages to install, if applicable to the artifact. '
            'By default all packages from the artifact are installed. '
            'Use any identifier which the package manager understands, e.g. openssh, openssh-8.2p1-73.f33, etc.'
        ),
    )
    install: bool = Field(
        True,
        description=('Flag to indicate if the artifact should be installed or just downloaded.'),
    )
    order: Optional[int] = Field(
        None,
        description=(
            'Order in which the artifact should be installed. '
            'By default the order is:\n'
            '* repository-file -10'
            '* repository - 20\n'
            '* fedora-copr-build - 30\n'
            '* redhat-brew-build - 40\n'
            '* fedora-koji-build - 50\n'
        ),
    )

    @root_validator
    def set_default_order(cls: Any, values: Any) -> Any:  # pylint: disable=invalid-name,no-self-argument,no-self-use
        """
        Set default `order` based on artifact `type`
        """
        if not values.get('order'):
            values['order'] = ARTIFACT_ORDER[values['type']]
        return values


class EnvironmentSettingsPipeline(NucleusBaseModel):
    """
    Various environment settings of pipeline
    """

    skip_guest_setup: bool = Field(
        False, description=('Skip guest setup playbooks. Can be handy to test guest setup playbooks.')
    )


class EnvironmentSettingsProvisioning(NucleusBaseModel):
    """
    Various environment setting of provisioning
    """

    post_install_script: Optional[str] = Field(
        None, description=('Pass a specific post-install-script to be used during guest provisioning.')
    )
    tags: Optional[Dict[str, str]] = Field(
        None,
        description=(
            'Dictionary of tags for labeling cloud resources provisioned for the test environment. '
            'Use `BusinessUnit: team_name` to tag resources for Testing Farm\'s cost reporting. '
            'The team name can be any string, but it is advised to use subsystem team '
            'name if applicable, e.g. `sst_cs_apps_rhel`, etc.'
        ),
    )


class EnvironmentSettings(NucleusBaseModel):
    """
    Various environment settings or tweaks.
    """

    pipeline: Optional[EnvironmentSettingsPipeline] = Field(None, description=('Pipeline settings or tweaks.'))
    provisioning: Optional[EnvironmentSettingsProvisioning] = Field(
        None, description=('Guest settings or tweaks to be applied during provisioning.')
    )


class Tmt(NucleusBaseModel):
    """
    Special environment settings for `tmt` tool.
    """

    context: Optional[Dict[str, str]] = Field(
        None,
        description=(
            'A mapping of tmt context variable names to values. For more information about tmt context '
            'see [documentation](https://tmt.readthedocs.io/en/latest/spec/context.html).'
        ),
    )
    environment: Optional[Dict[str, str]] = Field(
        None,
        description=(
            'A mapping of environment variables exposed to `tmt`. '
            'Used to configure various `tmt` plugins with possibly sensitive configuration. '
            'For example for the `polarion` report plugin, etc.'
        ),
    )


class Os(NucleusBaseModel):
    """
    Identifies the operating system used for the test environment.
    """

    compose: str = Field(
        ...,
        description=(
            'Specify the compose of the operating system to provision. '
            'Let Testing Farm choose the best infrastructure pool for execution. '
            'Both specific IDs and symbolic names can be used. '
            'Symbolic names are translated to specific IDs. '
            'List of composes can be found in the documentation: '
            'https://docs.testing-farm.io/Testing%20Farm/0.1/test-environment.html#_composes '
            'If you omit this field, the test execution will happen in a container. '
            'Container image can be optionally specified in the `tmt` plan. '
            'In the multihost pipeline, this field will be passed to `--update-missing` '
            'tmt option in the provision step.'
        ),
    )


class Kickstart(NucleusBaseModel):
    """
    Kickstart specification. See https://tmt.readthedocs.io/en/stable/spec/plans.html#kickstart for details.
    """

    kernel_options: Optional[str] = Field(
        None,
        alias='kernel-options',
        description='Kernel options are passed on the kernel command line when the installer is booted.',
    )
    kernel_options_post: Optional[str] = Field(
        None,
        alias='kernel-options-post',
        description='Post-install kernel options are set in the boot loader configuration, to be passed on the kernel '
        'command line after installation.',
    )
    metadata: Optional[str] = Field(
        None,
        alias='metadata',
        descrption='Special metadata information that modify the default kickstart template used by Beaker.',
    )
    post_install: Optional[str] = Field(
        None,
        alias='post-install',
        dscription="Corresponds to the '%post' section of a file. It can contain bash commands, this part is run after "
        'the installation of a guest.',
    )
    pre_install: Optional[str] = Field(
        None,
        alias='pre-install',
        decription="Corresponds to the '%pre' section of a file. It can contain bash commands, this part is run before "
        'the installation of a guest.',
    )
    script: Optional[str] = Field(
        None,
        alias='script',
        descripion='Contains the kickstart specific commands that are run during the installation of a guest.',
    )


class EnvironmentRequested(NucleusBaseModel):
    """
    Requested test environment to provision.
    """

    arch: Architectures = Field(
        ...,
    )
    os: Optional[Os] = Field(
        None,
    )
    pool: Optional[str] = Field(
        None,
        description=(
            'Name of the infrastructure pool to use. If not chosen, Testing Farm will choose the most suitable pool.'
        ),
    )
    variables: Optional[Dict[Any, Any]] = Field(
        None,
        description=(
            'A mapping of environment variable names to values, which will be exported in the test environment.'
        ),
    )
    secrets: Optional[Dict[Any, Any]] = Field(
        None,
        description=(
            'A mapping of secret environment variable names to values, which will be exported in the test '
            'environment. Testing Farm will hide the values from standard output of the tests and logs. If the tests '
            'generate their own log files, they are responsible for confidentiality of the secrets.'
        ),
    )
    artifacts: Optional[List[Artifact]] = Field(
        None, description=('Additional artifacts to install in the test environment.')
    )
    settings: Optional[EnvironmentSettings] = Field(
        None,
    )
    tmt: Optional[Tmt] = Field(
        None,
    )
    hardware: Optional[Dict[Any, Any]] = Field(
        None,
        description=(
            'Test environment hardware specification. '
            'See https://tmt.readthedocs.io/en/stable/spec/plans.html#hardware for details.'
        ),
    )
    kickstart: Optional[Kickstart] = Field(
        None,
        description=(
            'Kickstart specification. See https://tmt.readthedocs.io/en/stable/spec/plans.html#kickstart for details.'
        ),
    )


class EnvironmentProvisioned(NucleusBaseModel):
    """
    Provisioned test environment.
    """

    arch: Architectures = Field(
        ...,
    )
    os: Optional[Os] = Field(
        None,
    )
    pool: Optional[str] = Field(
        None,
        description=(
            'Name of the infrastructure pool to use. If not chosen, Testing Farm will choose the most suitable pool.'
        ),
    )


class Note(NucleusBaseModel):
    """
    Note produced by Testing Farm related.
    """

    level: NoteLevels = Field(...)
    message: str = Field(...)


class Stage(NucleusBaseModel):
    """
    Stage of the test request with various details.
    """

    name: str = Field(..., description=('Name of the stage.'))
    result: TestStageResults = Field(
        ...,
    )
    log: HttpUrl = Field(
        ..., description=('URL to a stage specific log. Can point also to an artifact directory with various logs.')
    )
    notes: Optional[List[Note]] = Field(
        None, description=('Notes produced by Testing Farm related to this concrete stage')
    )


class Webhook(NucleusBaseModel):
    """
    Notification webhook.
    """

    url: HttpUrl = Field(
        ...,
        description=(
            'Post to given webhook URL in case of the request has changed. The purpose of the webhook is to inform '
            'our users about request changes and mitigates the need of periodic polling for request updates. '
            'The body of the request contains the `request_id` and an optional authentication `token`. '
            'In case of unsuccessful request, the request is retried few times.'
        ),
    )
    token: Optional[str] = Field(
        '',
        description=(
            'Optional token to send in the body under key `token` when posting to the webhook URL. '
            'Provides means of authentication to the service accepting the webhook.'
        ),
    )


class Notification(NucleusBaseModel):
    """
    Request update notification settings.
    """

    webhook: Optional[Webhook] = Field(
        None,
    )


class Result(NucleusBaseModel):
    """
    Result related properties.
    """

    summary: Optional[str] = Field(
        None,
        description=(
            'Human readable summary of the test request. In case of error state contains the error message. '
            'In case of skipped results, contains the reason of the skipping. In case of passed or failed results '
            'in contains a human readable interpretation of the test results, e.g. 1 plans from 3 failed.'
        ),
    )
    overall: TestOverallResults = Field(
        ...,
    )
    xunit: Optional[str] = Field(None, description=('A string with test results in XUnit format.'))
    xunit_url: Optional[HttpUrl] = Field(None, description=('A URL link to test results in XUnit format.'))


class Run(NucleusBaseModel):
    """
    Details of the request run.
    """

    console: Optional[HttpUrl] = Field(
        None, description=('URL of a plain-text log from Testing Farm, which can be followed for progress.')
    )
    stages: Optional[List[Stage]] = Field(None, description=('Stages of the test request with various details.'))
    artifacts: HttpUrl = Field(..., description=('URL to the root of produced artifacts from the test request.'))


class RequestSettingsWorker(NucleusBaseModel):
    """
    Worker settings or tweaks.
    """

    image: Optional[str] = Field(
        None,
        description=(
            'Use given Testing Farm worker image instead of the one deployed in the specific ranch. '
            'Can be handy to test new releases of the worker image.'
        ),
    )


class RequestSettingsPipeline(NucleusBaseModel):
    """
    Settings that affect the pipeline.
    """

    timeout: Optional[int] = Field(
        None, description=('Timeout for the request in minutes. The default is 12h, i.e. 720 minutes.')
    )
    type: Optional[SettingsPipelineNames] = Field(
        None,
        description=(
            "Force given pipeline for testing. This field is used to gradually introduce new testing pipelines. "
            "Currently only accepted value is 'tmt-multihost'."
        ),
    )
    provision_error_failed_result: Optional[bool] = Field(
        False,
        alias='provision-error-failed-result',
        description=(
            "If any of the environments encounter an error during provisioning, the request won't be considered "
            "an error but will instead be `completed` with the `result.overall` field set to `failed`. "
            "This approach is beneficial when using Testing Farm to specifically test for successful provisioning."
        ),
    )
    parallel_limit: Optional[int] = Field(
        None,
        alias='parallel-limit',
        description=(
            "Maximum amount of plans to be executed in parallel. Default values are 12 for Public Ranch and 5 for "
            "Red Hat Ranch."
        ),
        gt=0,
        le=64,
    )


class RequestSettings(NucleusBaseModel):
    """
    Various request settings or tweaks.
    """

    worker: Optional[RequestSettingsWorker] = Field(None, description=('Worker settings or tweaks.'))
    pipeline: Optional[RequestSettingsPipeline] = Field(None, description=('Settings that affect the pipeline.'))


class RequestUserWebpage(NucleusBaseModel):
    """
    User webpage settings.
    """

    url: HttpUrl = Field(
        ...,
        description=("URL to the user's webpage. The link will be shown in the results viewer."),
        example='https://example.com',
    )
    icon: Optional[HttpUrl] = Field(
        None,
        description=("Icon of the user's webpage. The icon will be shown in the results viewer."),
        example='https://example.com/icon.png',
    )
    name: Optional[str] = Field(
        None,
        description=("Name of the user's webpage. The name will be shown in the results viewer."),
        example='Example CI, build #1234',
    )


class RequestUser(NucleusBaseModel):
    """
    User-related fields of the request.
    """

    webpage: Optional[RequestUserWebpage] = Field(None)


class RequestCreateIn(NucleusBaseModel):
    """
    Create test request API request.
    """

    api_key: str = Field(..., description=('An unique identifier used to authenticate a client.'))
    test: TestCreateIn = Field(
        ...,
    )
    environments: Optional[List[EnvironmentRequested]] = Field(
        None, description=('List of requested test environments.')
    )
    notification: Optional[Notification] = Field(
        None,
    )
    settings: Optional[RequestSettings] = Field(None)
    user: Optional[RequestUser] = Field(None)

    @validator('test')
    def test_one_of(cls: Any, v: Any) -> Any:  # pylint: disable=invalid-name,no-self-argument,no-self-use
        """
        Validate `test` has only one section.
        """
        counter = 0
        for key in v.__dict__.keys():
            if v.__dict__[key] is not None:
                counter += 1
        if counter > 1:
            raise UnprocessableEntityError(message='Test section has more than one type.')
        if counter < 1:
            raise UnprocessableEntityError(message='Test section is empty or test type is wrong.')
        return v


class RequestCreateOut(NucleusBaseModel):
    """
    Create test request API response.
    """

    id: uuid.UUID = Field(
        ...,
        description=(
            'A unique identification of the request. Uses UUID format as defined in '
            '[RFC 4122](https://tools.ietf.org/html/rfc4122). Generated by Testing Farm.'
        ),
    )
    test: Test = Field(
        ...,
    )
    # TODO: change once database migrated to enums with string values
    # state: RequestStateType = Field(
    state: str = Field(
        ...,
    )
    environments: Optional[List[EnvironmentRequested]] = Field(
        None, description=('List of requested test environments.')
    )
    notification: Optional[Notification] = Field(
        None,
    )
    settings: Optional[RequestSettings] = Field(None)
    user: Optional[RequestUser] = Field(None)
    created: datetime = Field(..., description=('Date/time of creation of the request in RFC 3339 format.'))
    updated: datetime = Field(..., description=('Date/time of last update of the request in RFC 3339 format.'))


class RequestGetUpdateOut(NucleusBaseModel):
    """
    Get and Update test request API response.
    """

    id: uuid.UUID = Field(
        ...,
        description=(
            'A unique identification of the request. Uses UUID format as defined in '
            '[RFC 4122](https://tools.ietf.org/html/rfc4122). Generated by Testing Farm.'
        ),
    )
    # NOTE: user_id is the same as token_id, it is preserved for backwards compatibility
    user_id: uuid.UUID = Field(..., description=('ID of the token which created the test request.'))
    token_id: uuid.UUID = Field(..., description=('ID of the token which created the test request.'))
    test: Test = Field(
        ...,
    )
    # TODO: change once database migrated to enums with string values
    # state: RequestStateType = Field(
    state: str = Field(
        ...,
    )
    environments_requested: Optional[List[EnvironmentRequested]] = Field(
        None, description=('List of requested test environments.')
    )
    notes: Optional[List[Note]] = Field(None, description=('Notes produced by Testing Farm.'))
    result: Optional[Result] = Field(
        None,
    )
    run: Optional[Run] = Field(
        ...,
    )
    settings: Optional[RequestSettings] = Field(None)
    user: Optional[RequestUser] = Field(None)

    queued_time: Optional[timedelta] = Field(..., description=('The duration of the request in the queue in seconds.'))
    run_time: Optional[timedelta] = Field(..., description=('The duration of the runtime in seconds.'))

    created: datetime = Field(..., description=('Date/time of creation of the request in RFC 3339 format.'))
    updated: datetime = Field(..., description=('Date/time of last update of the request in RFC 3339 format.'))


class RequestGetAuthenticatedOut(RequestGetUpdateOut):
    """
    Get test request with additional fields available after user authentication.
    """

    notification: Optional[Notification] = Field(
        None,
    )


class RequestUpdateIn(NucleusBaseModel):
    """
    Update test request API request.
    """

    api_key: str = Field(..., description=('An unique identifier used to authenticate a client.'))
    # TODO: change once database migrated to enums with string values
    # state: Optional[RequestStateType] = Field(
    state: Optional[TestStates] = Field(None)
    notes: Optional[List[Note]] = Field(None, description=('Notes produced by Testing Farm.'))
    environments_requested: Optional[List[EnvironmentProvisioned]] = Field(
        None, description=('List of provisioned test environments.')
    )
    result: Optional[Result] = Field(
        None,
    )
    run: Optional[Run] = Field(
        None,
    )
    notification: Optional[Notification] = Field(
        None,
    )
    settings: Optional[RequestSettings] = Field(None)
    user: Optional[RequestUser] = Field(None)


class RequestDeleteIn(NucleusBaseModel):
    """
    Delete test request API request.
    """

    api_key: str = Field(..., description=('An unique identifier used to authenticate a client.'))
