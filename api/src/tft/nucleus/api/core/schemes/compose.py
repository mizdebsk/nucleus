# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides schemes of test requests
"""
from typing import List

from pydantic import Field  # pylint: disable=no-name-in-module

from . import NucleusBaseModel


class Compose(NucleusBaseModel):
    """
    Testing compose
    """

    name: str = Field(..., description=('Name of the compose.'))


class SupportedComposesOut(NucleusBaseModel):
    """
    Get supported composes response
    """

    composes: List[Compose] = Field(..., description=('Composes supported by Testing Farm.'))
