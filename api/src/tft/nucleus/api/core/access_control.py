# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0

"""
The module provides functions for access control.
"""

import enum
import logging
from typing import Optional, cast
from uuid import UUID

import casbin
from sqlalchemy.orm import Session

from ..config import settings
from .database import Token, get_test_request_by_id, get_token_by_api_key
from .errors import ForbiddenError
from .log import log_access_attempt, setup_logger


class AccessObject(enum.Enum):
    """
    The class represents objects which can be accessed.
    """

    # Requests
    REQUEST = enum.auto()
    REQUEST_OWNED = enum.auto()
    REQUESTS_LIST = enum.auto()
    # Users
    USER = enum.auto()
    USER_SELF = enum.auto()
    USERS_LIST = enum.auto()
    # Tokens
    TOKEN = enum.auto()


class AccessAction(enum.Enum):
    """
    The class represents actions which can be performed on objects.
    """

    GET = enum.auto()
    GET_SECRETS = enum.auto()
    CREATE = enum.auto()
    UPDATE = enum.auto()
    DELETE = enum.auto()
    RESTART = enum.auto()


enforcer = casbin.Enforcer(settings.ACCESS_CONTROL_MODEL_PATH, settings.ACCESS_CONTROL_POLICY_PATH)

logger = setup_logger('access_control', settings.get('ACCESS_CONTROL_LOG_FILEPATH', None), log_level=logging.INFO)


def check_role_access(
    role: str, obj: AccessObject, action: AccessAction, no_error: bool = False, token: Optional[Token] = None
) -> bool:
    """
    Check if a user with provided role has access to the object with the action.
    """
    access = cast(bool, enforcer.enforce(role, obj.name.lower(), action.name.lower()))

    log_access_attempt(logger, role, obj.name.lower(), action.name.lower(), access, token)

    if access:
        return True

    if no_error:
        return False

    raise ForbiddenError


def check_access_api_key(
    session: Session, api_key: Optional[str], obj: AccessObject, action: AccessAction, no_error: bool = False
) -> bool:
    """
    Check if the provided api_key has access to the object with the action.
    """
    if api_key is None:
        return check_role_access("anonymous", obj, action, no_error=no_error)

    token = get_token_by_api_key(session, api_key)

    return check_role_access(token.role, obj, action, no_error=no_error, token=token)


def is_test_request_owned_by_token(session: Session, api_key: str, request_id: UUID) -> bool:
    """
    Check if a token with provided api_key has access to the object with the action.
    """
    token = get_token_by_api_key(session, api_key)
    test_request = get_test_request_by_id(session, request_id)

    return token.id == test_request.token_id
