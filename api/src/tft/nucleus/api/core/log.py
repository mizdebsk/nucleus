# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0

"""
The module provides logging functions.
"""
import logging
from typing import Optional

from logfmter import Logfmter  # type: ignore

from .database import Token


def setup_logger(name: str, log_filepath: Optional[str] = None, log_level: int = logging.DEBUG) -> logging.Logger:
    """
    Returns logger which logs to file or console.
    """

    logger = logging.getLogger(name)
    logger.setLevel(log_level)

    formatter = Logfmter(
        keys=["level", "when"],
        mapping={"level": "levelname", "when": "asctime"},
        datefmt="%Y-%m-%dT%H:%M:%S%z",
    )

    if log_filepath:
        file_handler = logging.FileHandler(log_filepath)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
    else:
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(formatter)
        logger.addHandler(console_handler)

    return logger


def log_access_attempt(
    logger: logging.Logger, role: str, obj: str, action: str, access: bool, token: Optional[Token] = None
) -> None:
    """
    Format and log access attempt.
    """

    extra = {
        "what": "access_control",
        "role": role,
        "object": obj,
        "action": action,
        "access": access,
    }
    if token:
        extra["token_id"] = token.id
        extra["token_name"] = token.name

    if access:
        logger.info("Access attempt", extra=extra)
    else:
        logger.warning("Access attempt", extra=extra)
