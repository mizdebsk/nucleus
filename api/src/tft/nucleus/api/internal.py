# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides public implementation of testing farm API.
"""
from fastapi import FastAPI
from sentry_sdk import set_tag
from starlette.responses import RedirectResponse

from .config import settings
from .core.errors import NucleusException, nucleus_exception_handler
from .v0_1.internal import internal_api_router_v0_1

set_tag('api', 'internal')

api = FastAPI(
    title='Testing Farm Private API',
)
api.include_router(internal_api_router_v0_1, prefix=settings.VERSIONS.V0_1.PREFIX)

api.add_exception_handler(NucleusException, nucleus_exception_handler)


@api.get('/', include_in_schema=False)
def redirect_to_docs() -> RedirectResponse:
    """
    The function redirects root endpoint to docs.
    """
    return RedirectResponse(url='/redoc')
