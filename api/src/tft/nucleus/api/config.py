# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides customized dynaconf settings.
"""

from dynaconf import Dynaconf

settings = Dynaconf(
    envvar_prefix="TF_API",
    settings_files=["settings.yaml"],
    environments=True,
)
