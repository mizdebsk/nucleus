#!/usr/bin/env python3
import collections
import json
import sys

import jq
import pendulum
import requests

DAYS_BEFORE = 14
HEADER = """
= Testing Farm Errors
:toc:

"""
ERRORS = collections.defaultdict(int)
URL_ID_PREFIX = "http://api.dev.testing-farm.io/v0.1/requests/"


def get_errors():
    """Query testing farm API using HTTP GET."""
    url = 'https://api.dev.testing-farm.io/v0.1/requests?state=error'

    response = requests.get(url)

    query = """
        .[] | {"id": .id, "created": .created, "error": .result.summary, "test": .test, "artifacts": .run.artifacts, "environments_requested": .environments_requested}
    """

    extracted_params = jq.jq(query).transform(response.json(), multiple_output=True)

    return extracted_params


def print_error(err):
    test = err["test"]
    env = err["environments_requested"][0]
    try:
        if test["fmf"]:
            test_url = test["fmf"]["url"]
            test_name = test["fmf"]["name"]
            test_ref = test["fmf"]["ref"]
        elif test["sti"]:
            test_url = test["sti"]["url"]
            test_name = test["sti"]["playbooks"]
            test_ref = test["sti"]["ref"]
        else:
            print(test)
            print('ERROR: fmf or tmt not found in "test" section')
            sys.exit(1)
    except (AttributeError, KeyError):
        print("Failed when parsing test: \n  {}".format(test))
        sys.exit(1)

    # in some cases, "os" is not defined, don't fail in such case
    try:
        compose = env["os"]["compose"]
    except TypeError:
        compose = "UnknownCompose"

    print(
        """
=== {} {} [{}]

* Id: {}{}
* Environment: {} @ {}
* Artifacts: {}
* Issue: {}
""".format(
            test_url,
            test_name or '',
            test_ref,
            URL_ID_PREFIX,
            err["id"],
            compose,
            env["arch"],
            err["artifacts"],
            err["error"],
        )
    )


if __name__ == '__main__':
    # /errors.py 2>/dev/null | asciidoctor -

    now = pendulum.now("utc")
    recent = now.subtract(days=DAYS_BEFORE)
    # set to previous midnight
    recent.replace(hour=0, minute=0, second=0, microsecond=0)
    recent_errors = []

    for err in get_errors():
        # adding timezone offset for correct comparison
        created = pendulum.parse(err["created"] + "+00:00")
        if created > recent:
            recent_errors.append((err, created))
            reason = err["error"]
            ERRORS[reason] += 1

    # sort errors by time, most recent first
    recent_errors.sort(reverse=True, key=lambda e: e[0]["created"])

    # format output
    print(HEADER)
    for i in range(0, DAYS_BEFORE):
        now = pendulum.now("utc")
        start_datetime = now.replace(hour=0, minute=0, second=0, microsecond=0)
        start_datetime = start_datetime.subtract(days=i)
        end_datetime = start_datetime.add(days=1)

        print("""== {}. {}.\n""".format(start_datetime.day, start_datetime.month))
        for (err, time) in recent_errors:
            if time >= start_datetime and time < end_datetime:
                print_error(err)

    top_ten = collections.Counter(ERRORS).most_common(10)
    print(
        """
== Top 10

{}""".format(
            '\n'.join(['* {} - {}'.format(error, count) for error, count in top_ten])
        )
    )
